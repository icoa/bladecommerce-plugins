<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 20:35
 */

namespace Plugins;

use Illuminate\Support\Facades\Event;

class ZanoxFrontend extends Plugin
{

    private $cmpCode = 100;

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "Plugins\\ZanoxFrontend@onFrontendFooter");
        Event::listen('frontend.template.head', "Plugins\\ZanoxFrontend@onTemplateHead");
    }


    function getPartnerId()
    {
        return \Input::get('zanpid', \Session::get('zanpid', null));
    }


    function onTemplateHead()
    {

        $scopeName = \FrontTpl::getScopeName();
        \Utils::log($scopeName, __METHOD__);
        $scope = \FrontTpl::getScope();
        \Utils::log($scope, __METHOD__);

        $zanpid = \Input::get('zanpid', null);
        if ($zanpid) {
            \Session::set('zanpid', $zanpid);
        }

        $params = [];
        $obj = null;

        switch ($scope) {
            case 'catalog':
                $category = $this->getCategory();
                if ($category) {
                    $params['category'] = $category->name;
                }
                break;
            case 'product':
                $obj = \FrontTpl::getData('model');
                if (!$obj) return;
                $params['identifier'] = $obj->id;
                $params['fn'] = $obj->name;
                if (isset($obj->category_name)) $params['category'] = $obj->category_name;
                if (isset($obj->brand_name)) $params['brand'] = $obj->brand_name;
                $params['description'] = ($obj->ldesc != '') ? $obj->ldesc : ($obj->getShortDescription() . ' - ' . $obj->attributes);
                $params['description'] = \Utils::stripTags($params['description']);
                $params['price'] = $obj->price_final;
                $params['amount'] = \Format::float($obj->price_final_raw);
                $params['currency'] = \FrontTpl::getData('currency_code');
                $params['photo'] = $obj->defaultImg;
                $params['url'] = $obj->link_absolute."?cmpCode=".$this->cmpCode;
                $params['language'] = \FrontTpl::getLang();

                break;
            default:

                break;
        }

        if (count($params) == 0) return;


        $str = '<!-- Zanox Master Tag Manager -->' . PHP_EOL;

        foreach ($params as $key => $value) {
            /*if ($key == 'identifier') {
                if ($obj AND $obj->ean13 != '') {
                    $str .= '<meta name="zx:' . $key . '" type="ean" content="' . $obj->ean13 . '">' . PHP_EOL;
                } else {
                    if ($obj AND $obj->sku != '') {
                        $str .= '<meta name="zx:' . $key . '" type="sku" content="' . $obj->sku . '">' . PHP_EOL;
                    } else {
                        $str .= '<meta name="zx:' . $key . '" content="' . $value . '">' . PHP_EOL;
                    }
                }

            } else {
                $str .= '<meta name="zx:' . $key . '" content="' . \Utils::quote($value) . '">' . PHP_EOL;
            }*/
            $str .= '<meta name="zx:' . $key . '" content="' . \Utils::quote($value) . '">' . PHP_EOL;
        }

        $str .= '<!-- EOF Zanox Master Tag Manager -->';

        return $str;
    }

    private function getCategory()
    {
        $scope = \FrontTpl::getScope();
        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->category_id > 0) {
                return \Category::getPublicObj($catalog->category_id);
            }
        }
        return null;
    }

    function onFrontendFooter()
    {
        $test = \Input::get('blade_test',null);
        if($test == 1)
            return null;
        $debug = \Config::get('app.debug', true);
        $rendering = 'generic';
        $scopeName = \FrontTpl::getScopeName();
        \Utils::log($scopeName, __METHOD__);
        $scope = \FrontTpl::getScope();
        \Utils::log($scope, __METHOD__);
        //$model = \FrontTpl::getData('model');

        switch ($scope) {
            case 'product':
            case 'home':
                $rendering = $scope;
                break;
            case 'catalog':
                $category = $this->getCategory();
                if ($category) {
                    $rendering = 'category';
                }
                break;
            case 'list':
                if ($scopeName == 'search-results') {
                    $rendering = 'search';
                }
                break;
        }

        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            $rendering = 'basket';
        }
        if ($scopeName == 'confirm_order') {
            $rendering = 'checkout';
        }
        if ($scopeName == 'register') {
            $rendering = 'registration';
        }


        $tagId = null;

        switch ($rendering) {
            case 'generic':
                $tagId = '7449D8D31F5C8ADE3415';
                break;
            case 'search':
                $tagId = '4A75F0DA6E011728EC50';
                break;
            case 'product':
                $tagId = '49C05F1A1CA91CD36776';
                break;
            case 'home':
                $tagId = '77D8C6F61221040E3B08';
                break;
            case 'category':
                $tagId = '324B1FCA3CB7728572BB';
                break;
            case 'checkout':
                $tagId = '7DB76D695252EDBDFF67';
                break;
            case 'basket':
                $tagId = '08DF9AAB76BBB2D3D180';
                break;
            case 'registration':
                $tagId = '19C641717571CFCF723B';
                break;
        }


        $inject = '';
        $lang = \FrontTpl::getLang();
        $currency = \FrontTpl::getData('currency_code');

        if ($scopeName == 'search-results') {
            $catalog = \Catalog::getObj();
            $q = e($catalog->q);

            $products_ids = $catalog->getProducts();
            //\Utils::log($products_ids,"PRODUCT ".__METHOD__);
            $products = [];
            if(count($products_ids)){
                foreach ($products_ids as $product_id) {
                    $product = \Product::getPublicObj($product_id);
                    if ($product) {
                        $product->setFullData(); //
                        $products[] = $this->wrapProduct($product, ['qty' => ($product->qty > 0 ? $product->qty : 0)]);
                    }
                }
            }

            $json = $this->toJson($products);
            $inject = <<<INJECT
<script type="text/javascript">
    var zx_search_query = "$q";
    var zx_language = "$lang";
    var zx_products = $json;
</script>
INJECT;

        }


        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            $products = \CartManager::getProducts();
            $wrappedProducts = [];
            if(count($products)){
                foreach ($products as $index => $product) {
                    $wrappedProducts[] = $this->wrapProduct($product, ['cart_quantity' => $product->cart_quantity]);
                }
            }
            $json = $this->toJson($wrappedProducts);
            $inject = <<<INJECT
<script type="text/javascript">
    var zx_language = "$lang";
    var zx_products = $json;
</script>
INJECT;
        }


        if ($scopeName == 'confirm_order') {
            $order = \FrontTpl::getData('order');
            $paymentState = \FrontTpl::getData('paymentState');
            if ($order and $paymentState and $paymentState->failed == 0) {
                $products = $order->getProducts();
                $wrappedProducts = [];
                if(count($products)){
                    foreach ($products as $index => $p) {
                        $obj = $p->product;
                        $obj->setFullData();
                        $wrappedProducts[] = $this->wrapProduct($obj, ['qty' => $p->product_quantity]);
                    }
                }

                $order_total = \Format::float($order->total_order - $order->total_shipping - $order->total_taxes);
                $customer_id = $order->customer_id;
                $order_id = $order->reference;


                $extra = "var CustomerID = \"$customer_id\";";
                $partner_id = $this->getPartnerId();
                if ($partner_id) {
                    $extra = PHP_EOL . "var PartnerID = \"$partner_id\";";
                } else {
                    $partner_id = 'XXX';
                }

                $json = $this->toJson($wrappedProducts);
                $inject = <<<INJECT
<script type="text/javascript">
    var zx_language = "$lang";
    var zx_products = $json;
    var zx_transaction = "$order_id";
    var zx_total_amount = "$order_total";
    var zx_total_currency = "$currency";
    $extra
</script>
<!-- START of the zanox affiliate HTML code -->
<!-- ( The HTML code may not be changed in the sense of faultless functionality! ) -->
<script type="text/javascript" src="https://ad.zanox.com/pps/?9668C736973346&mode=[[1]]&CustomerID=[[$customer_id]]&OrderID=[[$order_id]]&CurrencySymbol=[[$currency]]&TotalPrice=[[$order_total]]&PartnerID=[[$partner_id]]"></script>
<noscript>
<img src="https://ad.zanox.com/pps/?9668C736973346&mode=[[2]]&CustomerID=[[$customer_id]]&OrderID=[[$order_id]]&CurrencySymbol=[[$currency]]&TotalPrice=[[$order_total]]&PartnerID=[[$partner_id]]" width="1" height="1" />
</noscript>
<!-- ENDING of the zanox-affiliate HTML-Code -->
INJECT;
            } else {
                $rendering = false;
            }
        }


        if ($rendering == null OR $rendering == false) return;

        $page = ucfirst($rendering);

        $js = <<<TEXT
<!-- Mastertag Codes for Program: Kronoshop IT (id:9668) -->
$inject
<!-- Start of $page Page Mastertag Code: -->
<div class="zx_$tagId zx_mediaslot seohide">
    <script type="text/javascript">
        window._zx = window._zx || [];
        window._zx.push({"id":"$tagId"});
        (function(d) {
            var s = d.createElement("script"); s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//static.zanox.com/scripts/zanox.js";
            var a = d.getElementsByTagName("script")[0]; a.parentNode.insertBefore(s, a);
        }(document));
    </script>
</div>
<!-- End of $page Page Mastertag Code -->
TEXT;

        if ($debug) {
            $js = str_replace(['<script type="text/javascript">', '</script>'], ["<script type=\"text/javascript\">\n/* Prevent execution in debug mode", "*/\n</script>"], $js);
        }

        return $js;
    }


    public function wrapProduct($product, $extras = [])
    {

        $product_qty = 1;
        if (isset($extras['qty']))
            $product_qty = $extras['qty'];
        elseif (isset($product->cart_quantity))
            $product_qty = $product->cart_quantity;
        elseif (isset($product->qty))
            $product_qty = $product->qty;

        $ga_product = [
            'identifier' => $product->id,
            'amount' => \Format::float($product->price_final_raw),
            'currency' => \FrontTpl::getData('currency_code'),
            'quantity' => $product_qty
        ];

        return $ga_product;
    }

    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_SLASHES);
    }

}