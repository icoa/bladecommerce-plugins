<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 20:35
 */

namespace Plugins;

use Illuminate\Support\Facades\Event;

class ClerkFrontend extends Plugin
{

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "Plugins\\ClerkFrontend@onFrontendFooter");
    }


    function onFrontendFooter()
    {
        $scopeName = \FrontTpl::getScopeName();

        $debug = \Config::get('app.debug');

        $data = [];
        $inject = '';

        try {
            if ($scopeName == 'confirm_order') {
                $order = \FrontTpl::getData('order');
                $paymentState = \FrontTpl::getData('paymentState');
                if ($order and $paymentState and $paymentState->failed == 0) {
                    $products = $order->getProducts();
                    $wrappedProducts = [];
                    if (count($products)) {
                        foreach ($products as $index => $p) {
                            $obj = $p->product;
                            $obj->setFullData();
                            $wrappedProducts[] = $this->wrapProduct($obj, ['qty' => $p->product_quantity]);
                        }
                    }
                    $data['api'] = 'log/sale';
                    $data['sale'] = $order->id;
                    $data['customer'] = $order->customer_id;
                    $data['email'] = trim($order->customer->email);
                    $data['products'] = $this->toJson($wrappedProducts);
                    $clerkSpan = '<span class="clerk"';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $clerkSpan .= " data-{$key}='$value'";
                    }
                    $clerkSpan .= '></span>';
                    $inject .= $clerkSpan . PHP_EOL;
                }
            }
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(),__METHOD__);
        }


        $publicKey = trim(\Cfg::get('CLERK_PUBLIC_KEY'));
        if ($publicKey == null OR $publicKey == '') {
            return null;
        }


        $inject .= <<<INJECT
  <script type="text/javascript">
    window.clerkAsyncInit = function() {
        Clerk.config({
            key: '$publicKey'
        });
    };

    (function(){
        var e = document.createElement('script'); e.type='text/javascript'; e.async = true;
        e.src = document.location.protocol + '//api.clerk.io/static/clerk.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(e, s);
    })();
  </script>
INJECT;

        if ($debug) {
            //$inject = str_replace(['<script type="text/javascript">', '</script>'], ['<script type="text/javascript">/*', '*/</script>'], $inject);
        }

        return $inject;

    }


    public function wrapProduct($product, $extras = [])
    {

        $product_qty = 1;
        if (isset($extras['qty']))
            $product_qty = $extras['qty'];
        elseif (isset($product->cart_quantity))
            $product_qty = $product->cart_quantity;
        elseif (isset($product->qty))
            $product_qty = $product->qty;

        $ga_product = [
            'id' => $product->id,
            'price' => \Format::float($product->price_final_raw),
            //'currency' => \FrontTpl::getData('currency_code'),
            'quantity' => $product_qty
        ];

        return (object)$ga_product;
    }

    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }


}