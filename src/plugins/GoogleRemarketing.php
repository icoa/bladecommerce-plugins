<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 20:35
 */

namespace Plugins;

use Illuminate\Support\Facades\Event;
use services\GoogleRemarketingConfig;
use services\GoogleRemarketingLib;

class GoogleRemarketing extends Plugin
{

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "Plugins\\GoogleRemarketing@onFrontendFooter");
    }


    private function getCategory()
    {
        $scope = \FrontTpl::getScope();
        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->category_id > 0) {
                return \Category::getPublicObj($catalog->category_id);
            }
        }
        return null;
    }

    function onFrontendFooter()
    {

        try {
            $rendering = 'other';
            $scopeName = \FrontTpl::getScopeName();
            $scope = \FrontTpl::getScope();


            switch ($scope) {
                case 'product':
                case 'home':
                    $rendering = $scope;
                    break;
                case 'catalog':
                    $rendering = 'category';
                    break;
                case 'list':
                    if ($scopeName == 'search-results') {
                        $rendering = 'searchresults';
                    }
                    break;
            }

            if ($scopeName == 'checkout' OR $scopeName == 'cart') {
                $rendering = 'cart';
            }
            if ($scopeName == 'confirm_order') {
                $rendering = 'purchase';
            }

            $lang = \FrontTpl::getLang();

            $grl = new GoogleRemarketingLib($lang);

            $products_ids = null;
            $totalvalue = 0;
            if ($rendering == 'searchresults' OR $rendering == 'category') {
                $catalog = \Catalog::getObj();
                $products_ids = $catalog->getProducts();
            }
            if ($rendering == 'product') {
                $obj = \FrontTpl::getData('model');
                if ($obj) {
                    $products_ids = $obj->id;
                    $totalvalue = (float)\Format::float($obj->price_final_raw);
                }
            }
            if ($rendering == 'category') {
                $category = $this->getCategory();
                if ($category) $grl->addTagParam('category', $category->name);
                $catalog = \Catalog::getObj();
                $products_ids = $catalog->getProducts();
            }


            if ($rendering == 'cart') {
                $products = \CartManager::getProducts();
                if (count($products)) {
                    $products_ids = [];
                    foreach ($products as $index => $product) {
                        $products_ids[] = $product->id;
                    }
                    $totalvalue = (float)\Format::float(\CartManager::getTotalFinal());
                }
            }


            if ($scopeName == 'confirm_order') {
                $order = \FrontTpl::getData('order');
                $paymentState = \FrontTpl::getData('paymentState');
                if ($order and $paymentState and $paymentState->failed == 0) {
                    $products = $order->getProducts();
                    $products_ids = [];
                    if (count($products)) {
                        foreach ($products as $index => $p) {
                            $products_ids[] = $p->product->id;
                        }
                    }
                    $totalvalue = (float)\Format::float($order->total_order - $order->total_shipping - $order->total_taxes);
                } else {
                    $rendering = false;
                }
            }

            if ($rendering == null OR $rendering == false) return;

            if($products_ids){
                $grl->addTagParam('prodid',$products_ids);
            }
            if($totalvalue){
                $grl->addTagParam('totalvalue',$totalvalue);
            }
            return $grl->render($rendering);

        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($e->getTraceAsString(), __METHOD__);
        }

        return null;
    }


    public function wrapProduct($product, $extras = [])
    {

        $product_qty = 1;
        if (isset($extras['qty']))
            $product_qty = $extras['qty'];
        elseif (isset($product->cart_quantity))
            $product_qty = $product->cart_quantity;
        elseif (isset($product->qty))
            $product_qty = $product->qty;

        $ga_product = [
            'identifier' => $product->id,
            'amount' => \Format::float($product->price_final_raw),
            'currency' => \FrontTpl::getData('currency_code'),
            'quantity' => $product_qty
        ];

        return $ga_product;
    }

    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_SLASHES);
    }

}