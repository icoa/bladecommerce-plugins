<?php

namespace Plugins;

use Event;
use FrontTpl;
use CartManager;
use Catalog;
use FrontUser;
use Category;
use Brand;
use Collection;
use Product;
use Order;
use PaymentState;
use services\Traits\TraitParams;
use Illuminate\Support\Str;
use Config;
use Input;
use AttributeOption;

class Plugin{

    use TraitParams;

    protected $config_namespace = null;

    function registerEvents(){

    }

    /**
     * @param $name
     * @return bool
     */
    protected function isScopeNameOverridable($name){
        return in_array($name, ['register_activated']);
    }

    protected function setupFrontend()
    {
        $scopeName = FrontTpl::getScopeName();
        $scope = FrontTpl::getScope();
        $scopeId = FrontTpl::getScopeId();
        $lang = FrontTpl::getLang();
        $currency = FrontTpl::getData('currency_code');
        $user = FrontUser::get();
        $mobile = Config::get('mobile', false);
        $inputScopeName = Input::get('fe_scope_name');

        if(strlen($inputScopeName) > 0 and $this->isScopeNameOverridable($inputScopeName)){
            $scopeName = $inputScopeName;
        }

        $router = FrontTpl::getRouter();
        $url = null;
        if($router) {
            $url = $router->get('full');
        }

        $this->setParams(compact('scopeName', 'scope', 'scopeId', 'lang', 'currency', 'user', 'mobile', 'url'));
        //audit($this->getParams(), __METHOD__);
    }


    /**
     * @return Category|null
     */
    protected function getCatalogCategory()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            $catalog = Catalog::getObj();
            if ($catalog->category_id > 0) {
                return Category::getPublicObj($catalog->category_id);
            }
        }
        return null;
    }

    /**
     * @return Brand|null
     */
    protected function getCatalogBrand()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            $catalog = Catalog::getObj();
            if ($catalog->brand_id > 0) {
                return Brand::getPublicObj($catalog->brand_id);
            }
        }
        return null;
    }

    /**
     * @return Collection|null
     */
    protected function getCatalogCollection()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            $catalog = Catalog::getObj();
            if ($catalog->collection_id > 0) {
                return Collection::getPublicObj($catalog->collection_id);
            }
        }
        return null;
    }

    /**
     * @return AttributeOption|null
     */
    protected function getGenderCollection()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            $catalog = Catalog::getObj();
            if ($catalog->gender_id > 0) {
                return AttributeOption::getPublicObj($catalog->gender_id);
            }
        }
        return null;
    }

    /**
     * @return null|string
     */
    protected function getCatalogSearchQuery()
    {
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'search-results') {
            $catalog = Catalog::getObj();
            if (Str::length(trim($catalog->q)) > 0) {
                return trim($catalog->q);
            }
        }
        return null;
    }

    /**
     * @return array
     */
    protected function getCatalogProducts()
    {
        $scope = $this->getParam('scope');
        $scopeName = $this->getParam('scopeName');
        $products = [];
        if ($scope == 'catalog' or $scopeName == 'search-results') {
            $catalog = Catalog::getObj();
            $products_ids = $catalog->getProducts();
            if(is_array($products_ids) and !empty($products_ids)){
                foreach ($products_ids as $product_id) {
                    $product = Product::getFullProduct($product_id);
                    if ($product) {
                        $products[] = $product;
                    }
                }
            }
        }
        return $products;
    }

    /**
     * @return string|null
     */
    protected function getCatalogOrderBy()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            return Catalog::getOrderBy();
        }
        return null;
    }

    /**
     * @return string|null
     */
    protected function getCatalogOrderDir()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'catalog') {
            return Catalog::getOrderDir();
        }
        return null;
    }

    /**
     * @return Order|null
     */
    protected function getOrder()
    {
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'confirm_order') {
            return FrontTpl::getData('order');
        }
        return null;
    }


    /**
     * @return PaymentState|null
     */
    protected function getPaymentState()
    {
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'confirm_order') {
            return FrontTpl::getData('paymentState');
        }
        return null;
    }


    /**
     * @return array|null
     */
    protected function getCartProducts()
    {
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            return CartManager::getProducts();
        }
        return null;
    }


    /**
     * @return Product|null
     */
    protected function getProduct()
    {
        $scope = $this->getParam('scope');
        if ($scope == 'product') {
            return FrontTpl::getData('model');
        }
        return null;
    }

    /**
     * @return array
     */
    protected function getWishlistProducts(){
        $items = [];
        $rows = \FrontUser::getWishlists();
        if(is_array($rows) and !empty($rows)){
            foreach ($rows as $row) {
                $list = \Wishlist::find($row->id);
                $products = $list->getProducts();
                $size = count($products);
                if($size > 0){
                    foreach($products as $id){
                        $product = Product::getPublicObj($id);
                        if($product){
                            $items[] = $product;
                        }
                    }
                }
            }
        }
        return $items;
    }


    /**
     * @return \Cart|null
     */
    protected function getCart(){
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            return CartManager::getCurrentCart();
        }
        return null;
    }

    /**
     * @return float|int
     */
    protected function getCartTotal(){
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            return CartManager::getTotalFinal();
        }
        return null;
    }


    /**
     * @return float|int
     */
    protected function getCartDiscount(){
        $scopeName = $this->getParam('scopeName');
        if ($scopeName == 'checkout' OR $scopeName == 'cart') {
            return CartManager::getTotalCartDiscount();
        }
        return null;
    }

    /**
     * @param string $key
     * @param null $default
     * @return Config
     * @throws \Exception
     */
    protected function config($key = null, $default = null){
        if(is_null($this->config_namespace)){
            throw new \Exception("Config method requires a valid 'config_namespace', which is not provided in the class");
        }
        $prefix = is_null($key) ? 'plugins_settings.' . $this->config_namespace : 'plugins_settings.' . $this->config_namespace . '.' . $key;
        return config($prefix, $default);
    }
}