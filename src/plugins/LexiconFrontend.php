<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 20:35
 */

namespace Plugins;

use Illuminate\Support\Facades\Event;

class LexiconFrontend extends Plugin
{

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterHead', "Plugins\\LexiconFrontend@onFrontendHead");
    }


    function onFrontendHead()
    {
        $rows = \Lexicon::where('is_javascript', 1)->get();
        $matrix = [];
        foreach ($rows as $row) {
            $matrix[$row->code] = \Lex::get($row->code);
        }
        if (count($matrix) == 0) {
            return "";
        }
        // Setup our return array
        $js = array();

        $js[] = '<script>';


        $params = json_encode($matrix, JSON_UNESCAPED_SLASHES);
        $js[] = 'var Lexicon = ' . $params . ";";

        $js[] = '</script>';

        // Return our combined render
        return implode(PHP_EOL, $js);
    }


}