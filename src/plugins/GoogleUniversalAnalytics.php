<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 16:44
 */
namespace Plugins;

use Illuminate\Support\Facades\Event;

use Analytics;

class GoogleUniversalAnalytics extends Plugin
{


    public $list = 'catalog';

    function registerEvents()
    {
        parent::registerEvents();

        //FRONTEND
        Event::listen('frontend.template.afterHead', "Plugins\\GoogleUniversalAnalytics@onFrontendHead");
        Event::listen('frontend.template.afterFooter', "Plugins\\GoogleUniversalAnalytics@onFrontendFooter");
        Event::listen('frontend.product.afterList', "Plugins\\GoogleUniversalAnalytics@onProductAfterList");
        Event::listen('frontend.cart.addProduct', function (&$product, $qty) {
            return $this->onCartAddProduct($product, $qty);
        });
        Event::listen('frontend.cart.updateProduct', function (&$product, $qty) {
            return $this->onCartUpdateProduct($product, $qty);
        });
        Event::listen('frontend.cart.removeProduct', function (&$product, &$qty) {
            return $this->onCartRemoveProduct($product, $qty);
        });


        //BACKEND
        Event::listen('admin.template.afterHead', "Plugins\\GoogleUniversalAnalytics@onBackendHead");
        Event::listen('admin.template.afterFooter', "Plugins\\GoogleUniversalAnalytics@onBackendFooter");
    }


    function onBackendHead()
    {
        //\Utils::log("FIRING", __METHOD__);
        Analytics::ga('set', 'nonInteraction', true);
        return (Analytics::isActive() AND $this->isBackendValidScope()) ? Analytics::render() : null;
    }

    function onBackendFooter()
    {
        //\Utils::log("FIRING", __METHOD__);
        return (Analytics::isActive() AND $this->isBackendValidScope()) ? $this->renderBackendFooter() : null;
    }

    private function isBackendValidScope(){
        $scope = \AdminTpl::getScope();
        $valid = ['OrdersController','RmasController'];
        return (in_array($scope,$valid));
    }

    private function renderBackendFooter(){
        $js = [];
        $listeners = [];

        $listnersStr = 'Shared.listeners({';
        foreach ($listeners as $func => $listener) {
            $listnersStr .= "'$func':$listener,";
        }
        $listnersStr = rtrim($listnersStr, ",") . '});';

        $scope = \AdminTpl::getScope();
        if($scope == 'OrdersController'){
            $order = \AdminTpl::getData('order',null);
            if($order){
                $status = $order->getStatus();
                if($order->getSendFlag('purchase') == 0){
                    //\Utils::log("PURCHASE STATUS = 0 | STATUS ID: $status->id",__METHOD__);
                    if( $status AND $status->id == 5 ){ //order is closed
                        $products = $order->getProducts();
                        $wrappedProducts = [];

                        foreach ($products as $index => $detail) {
                            $product = $detail->product;
                            $wrappedProducts[] = $this->wrapProduct($product, ['qty' => $detail->product_quantity], $index, true);
                        }
                        $json = $this->toJson($wrappedProducts);
                        $js[] = "MBG.setCartProducts($json);";
                        $wrappedOrder = $this->wrapOrder($order);
                        $json = $this->toJson($wrappedOrder);
                        $js[] = "MBG.addTransaction($json);";
                        $order->setSendFlag('purchase',1);
                    }
                }

                if($order->getSendFlag('refund') == 0){
                    if( $status AND $status->id == 7 ){ //order is refunded
                        $products = $order->getProducts();
                        $wrappedProducts = [];

                        foreach ($products as $index => $detail) {
                            $product = $detail->product;
                            $wrappedProducts[] = $this->wrapProduct($product, ['qty' => $detail->product_quantity], $index, true);
                        }
                        $json = $this->toJson($wrappedProducts);
                        $js[] = "MBG.setCartProducts($json);";
                        $wrappedOrder = $this->wrapOrder($order);
                        $json = $this->toJson($wrappedOrder);
                        $js[] = "MBG.refundByOrderId($json);";
                        $order->setSendFlag('refund',1);
                    }
                }
            }
        }


        if($scope == 'RmasController'){
            $rmaLastStatus = \Session::get('rmaLastStatus',0);
            if($rmaLastStatus > 0){
                \Session::forget('rmaLastStatus');
                $rma = \AdminTpl::getData('rma',null);
                if($rma){
                    $statusObj = $rma->getStatus();
                    if($statusObj->confirmed == 1){
                        $rma_products = $rma->getProducts();
                        foreach($rma_products as $index => $rma_product){
                            $product = $rma_product->product;
                            $wrappedProducts[] = $this->wrapProduct($product, ['qty' => $rma_product->product_quantity], $index, true);
                        }
                        $json = $this->toJson($wrappedProducts);
                        $js[] = "MBG.setCartProducts($json);";
                        $order = $rma->getOrder();
                        $wrappedOrder = $this->wrapOrder($order);
                        $json = $this->toJson($wrappedOrder);
                        $js[] = "MBG.refundByOrderId($json);";
                        $order->setSendFlag('refund',1);
                    }
                }
            }
        }



        $currency = 'EUR';

        $jsPlain = implode(PHP_EOL, $js);

        $str = <<<STR
<script type="text/javascript">
function async(u, id, c) {
  var d = document, t = 'script', o = d.createElement(t), s = d.getElementsByTagName(t)[0];
  o.src = u;  o.async = true;
  if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
  s.parentNode.insertBefore(o, s);
}
async("/themes/admin/assets/js/echo/GoogleAnalyticActionLib.min.js",'GoogleAnalyticActionLib',function(){
    jQuery(document).ready(function(){
        var MBG = GoogleAnalyticEnhancedECommerce;
        MBG.setCurrency('$currency');
        $listnersStr
        $jsPlain
    });
});
</script>
STR;

        return $str;
    }


    function onFrontendHead()
    {
        //\Utils::log("FIRING", __METHOD__);
        return Analytics::isActive() ? Analytics::render() : null;
    }

    function onFrontendFooter()
    {
        //\Utils::log("FIRING", __METHOD__);
        if (Analytics::isActive() == false) {
            return null;
        }
        $test = \Input::get('blade_test',null);
        if($test == 1)
            return null;
        $this->setList();

        $js = [];
        $listeners = [
            'afterCatalogLoad' => 'MBG.afterCatalogLoad',
            'onCartAdd' => 'MBG.onCartAdd',
            'onCartUpdate' => 'MBG.onCartUpdate',
            'onCartDelete' => 'MBG.onCartDelete',
        ];


        $scope = \FrontTpl::getScopeName();

        \Utils::log($scope,__METHOD__);

        if ($scope == 'product') {
            $product = \FrontTpl::getData('model');
            $wrappedProduct = $this->wrapProduct($product, [], 1, true);
            $json = $this->toJson($wrappedProduct);
            $js[] = "MBG.addProductDetailView($json);";

            if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) > 0) {
                $js[] = "MBG.addProductClickByHttpReferal($json);";
            }
        }

        if ($scope == 'checkout' OR $scope == 'cart') {
            $products = \CartManager::getProducts();
            $wrappedProducts = [];
            $coupon = \CartManager::getCoupon();
            foreach ($products as $index => $product) {
                if ($coupon) {
                    $product->coupon = $coupon;
                }
                $wrappedProducts[] = $this->wrapProduct($product, ['cart_quantity' => $product->cart_quantity], $index, true);
            }
            $json = $this->toJson($wrappedProducts);
            $js[] = "MBG.setCartProducts($json);";

            $step = \CartManager::getStepInt();
            $stepName = \CartManager::getCheckoutStep();
            switch ($stepName) {
                case 'cart':
                    $option = "Cart #" . \CartManager::getTransactionId();
                    break;
                case 'auth':
                case 'reauth':
                    $option = false;
                    break;
                case 'shipping':
                    $option = "Guest checkout: " . (\CartManager::isGuestCheckout() ? 'YES' : 'NO');
                    break;
                case 'carrier':
                    //$option = "Carrier: ".\CartManager::getCarrierName();
                    $option = false;
                    break;
                case 'confirm':
                    $option = "Payment: " . \CartManager::getPaymentName();
                    break;
            }
            $json = $this->toJson(['step' => $step, 'option' => $option]);
            $js[] = "MBG.addCheckout($json);";

            $listeners['onCartOptionUpdate'] = 'MBG.onCartOptionUpdate';
            $listeners['onCheckoutOptionUpdate'] = 'MBG.onCartOptionUpdate';
            $listeners['onCheckoutStep'] = 'MBG.onCheckoutStep';
        }

        if ($scope == 'confirm_order') {
            $order = \FrontTpl::getData('order');
            $paymentState = \FrontTpl::getData('paymentState');
            $payment = ($order) ? $order->getPayment() : null;
            if($payment and $payment->online == 1){
                if ($paymentState and $paymentState->failed == 0) {
                    $products = $order->getProducts();
                    $wrappedProducts = [];
                    foreach ($products as $index => $p) {
                        $obj = $p->product;
                        $obj->setFullData();
                        $wrappedProducts[] = $this->wrapProduct($obj, ['qty' => $p->product_quantity], $index, true);
                    }

                    $wrappedOrder = $this->wrapOrder($order);
                    if($wrappedOrder){
                        $json = $this->toJson($wrappedProducts);
                        $js[] = "MBG.setCartProducts($json);";
                        $json = $this->toJson($wrappedOrder);
                        $js[] = "MBG.addTransaction($json);";
                        $order->setSendFlag('purchase');
                    }
                }
            }
        }


        if($scope == 'promotions'){
            $promotions = \FrontTpl::getData('promotions');
            if($promotions){
                $wrappedPromotions = [];
                foreach($promotions as $promo){
                    $wrappedPromotions[] = $this->wrapPromotion($promo);
                }
                $json = $this->toJson($wrappedPromotions);
                $js[] = "MBG.addPromotions($json);";
            }
        }



        $listnersStr = 'Shared.listeners({';
        foreach ($listeners as $func => $listener) {
            $listnersStr .= "'$func':$listener,";
        }
        $listnersStr = rtrim($listnersStr, ",") . '});';

        $js[] = 'ga("send", "pageview");';

        $currency = \FrontTpl::getData('currency_code');

        $jsPlain = implode(PHP_EOL, $js);

        /*$str = <<<STR
<script type="text/javascript">
function async(u, id, c) {
  var d = document, t = 'script', o = d.createElement(t), s = d.getElementsByTagName(t)[0];
  o.src = u;  o.async = true;
  if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
  s.parentNode.insertBefore(o, s);
}
async("/themes/admin/assets/js/echo/GoogleAnalyticActionLib.min.js",'GoogleAnalyticActionLib',function(){
    jQuery(document).ready(function(){
        var MBG = GoogleAnalyticEnhancedECommerce;
        MBG.setCurrency('$currency');
        MBG.init(false);
        $listnersStr
        $jsPlain
    });
});
</script>
STR;*/

        $str = <<<STR
<!-- Google Analytics Enhanced Ecommerce -->
<script type="text/javascript">
    var f = function(){
        try{
            var MBG = GoogleAnalyticEnhancedECommerce;
            MBG.setCurrency('$currency');
            MBG.init(false);
            $listnersStr
        }catch(e){console.log(e);}
        $jsPlain
    };
    if(window.jQuery){
        jQuery(document).ready(f);
    }else{
        defer_functions.push(f);
    }
</script>
<!-- EOF :: Google Analytics Enhanced Ecommerce -->
STR;

        return $str;
    }


    function setList()
    {
        $scope = \FrontTpl::getScopeName();
        $scope_id = \FrontTpl::getScopeId();
        if ($scope == 'product') {
            $this->list = $scope . "-" . $scope_id;
        } else {
            $this->list = $scope;
        }
    }


    function onProductAfterList($product, $counter)
    {
        //\Utils::log("FIRING", __METHOD__);
        $this->setList();

        if (Analytics::isActive()) {
            if ($product instanceof Product)
                $product = $product->toArray();

            $wrappedProduct = $this->wrapProduct($product, [], $counter + 1, true);
            $json = $this->toJson($wrappedProduct);
            $js = "<script id='pal_script_{$product->id}'>GAEE.products.push($json);</script>";
            return $js;
        }
    }


    function onCartAddProduct(\Product &$product, $qty)
    {
        //\Utils::log("FIRING", __METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $qty], 0, true);
        return $productArray;
    }


    function onCartUpdateProduct(\Product &$product, $qty)
    {
        //\Utils::log("FIRING", __METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $qty], 0, true);
        return $productArray;
    }


    function onCartRemoveProduct(\Product &$product, &$qty)
    {
        //\Utils::log("FIRING", __METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $productArray['cart_quantity']], 0, true);
        $qty = $productArray['quantity'];
        return $productArray;
    }


    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_SLASHES);
    }


    /**
     * wrap products to provide a standard products information for google analytics script
     */
    public function wrapProducts($products, $extras = array(), $full = false)
    {
        $result_products = array();
        if (!is_array($products))
            return;


        foreach ($products as $index => $product) {
            if ($product instanceof Product)
                $product = $product->toArray();


            $result_products[] = $this->wrapProduct($product, $extras, $index, $full);
        }

        return $result_products;
    }


    /**
     * wrap product to provide a standard product information for google analytics script
     */
    public function wrapProduct($product, $extras, $index = 0, $full = false)
    {
        $ga_product = '';

        if (is_object($product)) {
            $product = $product->toArray();
        }

        $variant = null;
        if (isset($product['attributes_small']))
            $variant = $product['attributes_small'];
        elseif (isset($extras['attributes_small']))
            $variant = $extras['attributes_small'];

        $product_qty = 1;
        if (isset($extras['qty']))
            $product_qty = $extras['qty'];
        elseif (isset($product['cart_quantity']))
            $product_qty = $product['cart_quantity'];

        if ($full) {
            $product_id = 0;
            if (!empty($product['sku']))
                $product_id = $product['sku'];
            else if (!empty($product['id']))
                $product_id = $product['id'];

            $product_type = 'typical';
            if (isset($product['pack']) && $product['pack'] == 1)
                $product_type = 'pack';
            elseif (isset($product['virtual']) && $product['virtual'] == 1)
                $product_type = 'virtual';

            $ga_product = array(
                '_id' => $product['id'],
                'id' => $product_id,
                'name' => $product['name'],
                'category' => $product['full_category_name'],
                'category_id' => $product['default_category_id'],
                'brand' => isset($product['brand_name']) ? $product['brand_name'] : '',
                'variant' => $variant,
                'type' => $product_type,
                'position' => $index ? $index : '0',
                'quantity' => $product_qty,
                'list' => $this->list,
                'url' => isset($product['link_absolute']) ? $product['link_absolute'] : '',
                'price' => number_format($product['price_final_raw'], 2)
            );

            //$ga_product = array_map('urlencode', $ga_product);
        }

        return $ga_product;
    }


    /**
     * Return a detailed transaction for Google Analytics
     */
    public function wrapOrder($order)
    {
        if (is_numeric($order)) {
            $order = \Order::getObj($order);
        }

        if(!is_object($order)){
            return null;
        }

        $data = array(
            'id' => $order->reference,
            'affiliation' => \Cfg::get('SITE_TITLE'),
            'revenue' => number_format($order->total_order,2),
            'shipping' => number_format($order->total_shipping,2),
            'tax' => number_format($order->total_taxes,2)
        );
        if ($order->coupon_code != '') {
            $data['coupon'] = $order->coupon_code;
        }
        return $data;
    }



    /**
     * Return a detailed promotion for Google Analytics
     */
    public function wrapPromotion($promo, $extra = [])
    {
        if (is_numeric($promo)) {
            $promo = \PriceRule::getObj($promo);
        }

        if(!is_object($promo)){
            return null;
        }

        $data = array(
            'id' => 'PROMO_'.$promo->id,
            'name' => $promo->name,
        );

        $eligible = ['creative','position'];
        foreach($eligible as $key => $field){
            if(isset($extra[$key])){
                $data->$key = $field;
            }
        }

        return $data;
    }

}