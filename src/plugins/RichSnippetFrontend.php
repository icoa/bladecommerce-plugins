<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 07/03/2015
 * Time: 20:35
 */

namespace Plugins;

use Illuminate\Support\Facades\Event;

class RichSnippetFrontend extends Plugin
{

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.product.afterProductPage', "Plugins\\RichSnippetFrontend@onAfterProductPage");
        Event::listen('frontend.template.head', "Plugins\\RichSnippetFrontend@onTemplateHead");
    }


    function onTemplateHead()
    {


        $scope = \FrontTpl::getScope();
        if ($scope != 'product') return;
        /** @var \Product $obj */
        $obj = \FrontTpl::getData('model');
        if (!$obj) return;


        $name = $obj->name;
        $sku = $obj->sku;
        $ean13 = $obj->ean13;
        $link = $obj->link_absolute;
        $currency = \FrontTpl::getData('currency_code');

        $img = $obj->defaultImg;
        $imageList = [];
        $images = $obj->getImages();
        foreach ($images as $image) {
            $imageList[] = $image->defaultImg;
        }
        if (!empty($imageList)){
            $imageList = array_map(function($el){
                return \Site::img($el, true);
            }, $imageList);
            $img = $imageList;
        }

        $price = number_format($obj->price_final_raw, 2, '.', '');
        $sdesc = \Str::length(trim($obj->sdesc) > 0) ? trim(strip_tags($obj->sdesc)) : "$obj->name - $obj->attributes";

        $name = \Utils::xml_entities($name);
        $sdesc = \Utils::xml_entities($sdesc);
        $availability = $obj->availabilitySchemaValue;
        $itemCondition = 'http://schema.org/' . ucfirst($obj->condition) . 'Condition';
        $seller = \Cfg::get('SITE_TITLE');
        $priceValidUntil = \Carbon\Carbon::now()->addMonth()->format('Y-m-d');

        $brand_schema = [];
        if ($obj->brand_id > 0) {
            $brand_logo = \Site::img($obj->brand_image_default, true);
            $brand_name = \Utils::xml_entities($obj->brand_name);
            $brand_schema = [
                '@type' => 'Brand',
                'name' => $brand_name,
                'logo' => $brand_logo,
            ];
        }

        $schema = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'name' => $name,
            'image' => $img,
            'description' => $sdesc,
            'url' => $link,
            'sku' => $sku,
            'mpn' => $sku,
            'gtin13' => $ean13,
        ];

        if (!empty($brand_schema)) {
            $schema['brand'] = $brand_schema;
        }

        $schema['offers'] = [
            '@type' => 'Offer',
            'priceValidUntil' => $priceValidUntil,
            'url' => $link,
            'itemCondition' => $itemCondition,
            'availability' => $availability,
            'price' => $price,
            'priceCurrency' => $currency,
            'seller' => [
                '@type' => 'Organization',
                'name' => $seller,
            ]
        ];

        $schema_str = json_encode($schema, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        $str = <<<TPL
<script type="application/ld+json">
$schema_str
</script>
TPL;
        return $str;
    }


    function onAfterProductPage($obj)
    {
        try {
            $status = 'unknown';
            if ($obj->availabilityStatus == 'in_stock') {
                $status = 'instock';
            }
            if ($obj->availabilityStatus == 'out_of_stock') {
                $status = 'outofstock';
            }
            if ($obj->availabilityStatus == 'preorder') {
                $status = 'outofstock';
            }
            if ($obj->outlet == 1) {
                $status = 'outlet';
            }
            if ($obj->is_out_of_production == 1) {
                $status = 'outofproduction';
            }
            $offer = 'regular';
            if ($obj->price_saving_raw > 0) {
                $offer = 'discount';
            }

            $str = <<<TPL
<script>
try{
    ga('send', 'event', 'Product Page', 'status', '$status');
    ga('send', 'event', 'Product Page', 'offer', '$offer');
}catch(e){}
</script>
TPL;
            return $str;
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
        }

    }


}